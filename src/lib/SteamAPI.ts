export type PriceResponse = { lastPrices: Array<number>, time: string, success: boolean }

export const getPrice = async (name: string):Promise<PriceResponse> =>
{
    const url = `https://steamcommunity.com/market/listings/730/${encodeURIComponent(name)}/render/?query=&start=` +
        "0&count=10&country=RU&language=russian&currency=5";
    const response = await fetch(url)
    if ( response.status != 200)
    {
        return { success: false, lastPrices: [], time: "" }
    }
    const body = await response.json()
    const prices: Array<number> = []
    for (const listing of Object.values(body.listinginfo))
    {
        const price = calcPrice(listing)
        if ( !Number.isNaN(price))
        {
            prices.push(price)
        }
    }
    return { success: true, lastPrices: prices, time: new Date().toLocaleTimeString() }

}

const calcPrice = (listing): number =>
{
    return (listing.converted_fee + listing.converted_price)/100
}