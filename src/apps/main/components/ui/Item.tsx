import {
    Button,
    Grid,
    GridSize,
    TextField,
    Typography,
    Tooltip,
    styled,
    TooltipProps, tooltipClasses
} from "@mui/material";
import React from "react";
import { PriceResponse } from "lib";


export function ItemColumn(props: { xs?: GridSize, children?: Array<any> | any , onClick?: any})
{
    return <Grid item xs={props.xs} onClick={props.onClick}>
        <Typography component={"span"} variant={"body1"} align={"center"} display={"block"}>
            { props.children }
        </Typography>
    </Grid>
}

export type PricesDict = Record<string, PriceResponse>

type InputProps = {
    value: number,
    step: number,
    onBlur: (event) => void
}

export function Input(props: InputProps)
{
    const [isVisible, setVisible] = React.useState<boolean>(false);

    const changeState = () =>
    {
        setVisible(!isVisible)
    };

    if (isVisible)
    {
        return <TextField defaultValue={props.value}
                          size={"small"}
                          type={"number"}
                          inputProps={{ step: props.step }}
                          variant={"outlined"}
                          onMouseLeave={(e) =>
                          {
                              props.onBlur(e);
                              changeState()
                          }}
                          autoFocus={true}
        />
    }
    return <Button onClick={changeState}>{props.value}</Button>
}

export const HtmlTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
        backgroundColor: '#f5f5f9',
        color: 'rgba(0, 0, 0, 0.87)',
        maxWidth: 220,
        fontSize: theme.typography.pxToRem(12),
        border: '1px solid #dadde9',
    },
}));