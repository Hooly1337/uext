import React from "react"
import { Grid, GridSize, Typography } from "@mui/material";

type HeaderProps = {
    children: Array<any> | any,
    xs: GridSize,
    onClick?: any
}

export function Header(props: HeaderProps) {
    return <Grid item xs={props.xs} onClick={props.onClick}>
        <Typography align={"center"} className={"header"} variant={"h6"}>
            {props.children}
        </Typography>
    </Grid>
}