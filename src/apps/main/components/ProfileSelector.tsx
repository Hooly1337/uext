import React from "react"
import { Grid, IconButton, MenuItem, Select, TextField } from "@mui/material";
import { Add } from "@mui/icons-material";
import { useAtom, useAtomValue } from "jotai";
import { currentVariantNameAtom, profileAtom, VariantName } from "store";
import { FloatTable } from "./variants/Float";
import { StickerTable } from "./variants/Sticker";


export default function ProfileSelector() {

    const [profiles, updateProfiles] = useAtom(profileAtom);

    const currentVariantName = useAtomValue(currentVariantNameAtom);

    const [profileInput, setProfileInput] = React.useState<string>("")

    const handleChangeSelect = event => {
        updateProfiles({ ...profiles, current: event.target.value });
    }

    const handleCreateProfile = (event: "typing" | "create") => {
        switch (event) {
            case "create":
                return () => {
                    profiles.all.push(profileInput)
                    profiles.current = profileInput
                    updateProfiles({ ...profiles })
                }
            case "typing":
                return (event) => {
                    setProfileInput(event.target.value)
                }
        }
    }

    const renderChosen = (variant: VariantName) => {
        switch (variant) {
            case "Floats": return <FloatTable/>
            case "NLStickers": return <StickerTable/>
        }
    }

    return <>
        <Grid container alignItems="flex-end" spacing="2">
            <Grid item>
                <Select value={profiles.current} onChange={handleChangeSelect}>
                    <MenuItem value={"new"}>
                        Create new
                    </MenuItem>
                    {profiles.all.map((value, index) => {
                        return <MenuItem value={value} key={index}>{value}</MenuItem>
                    })}
                </Select>
            </Grid>
            {profiles.current == "new" &&
                <>
                    <Grid item>
                        <TextField label={"Profile name"} onChange={handleCreateProfile("typing")}/>
                    </Grid>
                    <Grid item>
                        <IconButton component={"span"} size={"small"} onClick={handleCreateProfile("create")}>
                            <Add/>
                        </IconButton>
                    </Grid>
                </>
            }
        </Grid>
        {profiles.current != "new" &&
            renderChosen(currentVariantName)
        }
    </>

}