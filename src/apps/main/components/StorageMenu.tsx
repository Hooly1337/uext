import React, { Profiler, useEffect } from "react"
import { Button, Grid, TextField } from "@mui/material";
import { currentProfileStorageNameAtom, currentVariantNameAtom, FloatItem, ItemDict, NLStickerItem } from "store";
import { useAtom, useAtomValue, WritableAtom } from "jotai";


export function StorageMenu({ itemsAtom }: { itemsAtom: WritableAtom<ItemDict<FloatItem | NLStickerItem>, ItemDict<FloatItem | NLStickerItem>> }) {
    const [items, setItems] = useAtom(itemsAtom);

    const [importText, setImportText] = React.useState<string>("")

    const storageName = useAtomValue(currentProfileStorageNameAtom);

    const currentVariantName = useAtomValue(currentVariantNameAtom);


    useEffect(() => {
        console.log(itemsAtom)
    }, [itemsAtom])

    const setNewBase = () => {
        try {
            const prepared = {};
            const base = JSON.parse(importText);

            switch (currentVariantName) {
                case "NLStickers": {
                    for (const itemName in base) {
                        const item: NLStickerItem = base[itemName];
                        prepared[itemName] = {
                            name: item.name,
                            maxPricePercent: +item.maxPricePercent,
                            streakPrice: item.streakPrice.map(item => +item)
                        }
                    }
                    break;
                }
                case "Floats": {
                    for (const itemName in base) {
                        const item: FloatItem = base[itemName];
                        prepared[itemName] = {
                            name: item.name,
                            minFloat: +item.minFloat,
                            maxFloat: +item.maxFloat,
                            maxPrice: +item.maxPrice
                        }
                    }
                    break;
                }
            }

            setItems(prepared);
        } catch (e) {
            console.log(e);
        }
    }

    type Events = "import" | "typing" | "export" | "clear" | "enter";
    const handleEvents = (event: Events) => {
        switch (event) {
            case "export":
                return async () => {
                    const blob = new Blob([JSON.stringify(items)]);
                    const currentDate = new Date();
                    chrome.downloads.download({
                        url: URL.createObjectURL(blob),
                        filename: `${storageName} ${currentDate.getDate()}${currentDate.getMonth()}.json`
                    })
                    console.log("export");

                }
            case "typing":
                return (event) => {
                    console.log(event);
                    setImportText(event.target.value)
                }
            case "clear":
                return () => {
                    console.log(event);
                    setItems({})
                }
            case "import":
                return () => {
                    console.log(event);
                    setNewBase()
                }
            case "enter":
                return (e) => {
                    if (e.key === "Enter") {
                        console.log(e);
                        setNewBase()
                    }
                }
        }
    }

    return (
        <Grid item container>
            <Grid item>
                <TextField variant={"outlined"}
                           onChange={handleEvents("typing")}
                           size={"small"}
                           label={"Base.."}
                           value={importText}
                           onKeyDown={handleEvents("enter")}
                />
            </Grid>
            <Grid item>
                <Button onClick={handleEvents("import")}>Import</Button>
            </Grid>
            <Grid item>
                <Button onClick={handleEvents("export")}>Export</Button>
            </Grid>
            <Grid>
                <Button onClick={handleEvents("clear")}>Clear</Button>
            </Grid>
        </Grid>
    )
}