import React from "react";
import { Button, Grid, IconButton } from "@mui/material";
import { HtmlTooltip, Input, ItemColumn } from "../../ui";
import { ItemChanges } from "./StickerTable";
import { Launch, Update, DeleteForever } from "@mui/icons-material";
import { PriceResponse } from "lib";


type StickerItemProps = {
    name: string,
    maxPricePercent: number,
    streakPrice: number[],
    handleEvents: (type: ItemChanges) => (event) => void,
    currentPrice: PriceResponse
}

export function NLStickerRow(props: StickerItemProps) {

    const handleCopy = async () => {
        await navigator.clipboard.writeText(props.name)
    };

    const displayCurrentPrice = () => {
        if (props.currentPrice) {
            return <HtmlTooltip
                arrow
                title={<Grid container direction={"column"}>
                    <Grid item>
                        <em>{props.currentPrice.time}</em>
                    </Grid>
                </Grid>}>
                <Button onClick={props.handleEvents("updatePrice")}>{props.currentPrice.lastPrices[0]}</Button>
            </HtmlTooltip>
        }
        return <IconButton onClick={props.handleEvents("updatePrice")}>
            <Update/>
        </IconButton>
    };

    const link = `https://steamcommunity.com/market/listings/730/${encodeURI(props.name)}`;
    return <Grid>
        <ItemColumn xs={3}>
            <Button onClick={handleCopy}>{props.name}</Button>
        </ItemColumn>
        <ItemColumn xs={1}>
            <IconButton href={link} target={"_blank"}>
                <Launch fontSize={"small"}/>
            </IconButton>
        </ItemColumn>
        <ItemColumn xs={2}>
            {displayCurrentPrice()}
        </ItemColumn>
        <ItemColumn xs={1}>
            <Input onBlur={props.handleEvents("maxPricePercent")}
                   value={props.maxPricePercent}
                   step={1}/>
        </ItemColumn>
        <ItemColumn xs={1}>
            <Input onBlur={props.handleEvents("maxPrice0")}
                   value={props.streakPrice[0]}
                   step={10}/>
        </ItemColumn>
        <ItemColumn xs={1}>
            <Input onBlur={props.handleEvents("maxPrice1")}
                   value={props.streakPrice[1]}
                   step={10}/>
        </ItemColumn>
        <ItemColumn xs={1}>
            <Input onBlur={props.handleEvents("maxPrice2")}
                   value={props.streakPrice[2]}
                   step={10}/>
        </ItemColumn>
        <ItemColumn xs={1}>
            <Input onBlur={props.handleEvents("maxPrice3")}
                   value={props.streakPrice[3]}
                   step={10}/>
        </ItemColumn>
        <ItemColumn xs={1}>
            <IconButton onClick={props.handleEvents("delete")}>
                <DeleteForever fontSize={"small"}/>
            </IconButton>
        </ItemColumn>
    </Grid>
}
