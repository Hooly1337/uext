import React from "react";
import { debounce, Grid } from "@mui/material";
import { Header } from "../../ui";
import { StorageMenu } from "../../StorageMenu";
import { NLStickerRow } from "./NLStickerRow";
import { ItemInfo } from "../../../../content/main";
import { PricesDict } from "../../ui";
import { getPrice } from "lib";
import { currentProfileStorageNameAtom, NLStickersAtom } from "store";
import { useAtom, useAtomValue } from "jotai";

type Percent = number
type StickerItemStored = {
    name: string,
    maxPricePercent: Percent, // процент от дефа - максимальная цена
    streakPrice: [number, number, number, number] // абсолютная максимальная цена в зависимости от стрика
}
export type ItemChanges = "delete" | "maxPricePercent" | "updatePrice" | "maxPrice0" | "maxPrice1" |
    "maxPrice2" | "maxPrice3"

export function StickerTable() {
    const [items, setItems] = useAtom(NLStickersAtom);

    const currentProfileStorageName = useAtomValue(currentProfileStorageNameAtom);

    const [prices, setPrices] = React.useState<PricesDict>({});

    const saveItems = debounce(() => {
        localStorage.setItem(currentProfileStorageName, JSON.stringify(items))
    }, 2000);

    const handleItemChanges = (name: string) => {
        return (event: ItemChanges) => {
            switch (event) {
                case "delete" :
                    return () => {
                        delete items[name];
                        saveItems();
                        setItems(items)
                    };
                case "maxPricePercent":
                    return (event) => {
                        items[name].maxPricePercent = parseFloat(event.target.value);
                        saveItems();
                        setItems(items)
                    };
                case "updatePrice":
                    return async () => {
                        prices[name] = await getPrice(name);
                        setPrices({ ...prices })
                    };
                case "maxPrice0":
                    return (event) => {
                        items[name].streakPrice[0] = parseFloat(event.target.value);
                        saveItems();
                        setItems(items)
                    };
                case "maxPrice1":
                    return (event) => {
                        items[name].streakPrice[1] = parseFloat(event.target.value);
                        saveItems();
                        setItems(items)
                    };
                case "maxPrice2":
                    return (event) => {
                        items[name].streakPrice[2] = parseFloat(event.target.value);
                        saveItems();
                        setItems(items)
                    };
                case "maxPrice3":
                    return (event) => {
                        items[name].streakPrice[3] = parseFloat(event.target.value);
                        saveItems();
                        setItems(items)
                    };
            }
        }
    };

    const contentListener = () => {
        const listener = (itemInfo: ItemInfo) => {
            if (!items[itemInfo.name]) {
                const item: StickerItemStored =
                    {
                        name: itemInfo.name,
                        maxPricePercent: 0,
                        streakPrice: [0, 0, 0, 0]
                    };
                items[item.name] = item;
                saveItems();
                setItems(items)
            }
        };
        //chrome.runtime.onMessage.addListener(listener);
        return () => {
            //chrome.runtime.onMessage.removeListener(listener)
        }
    };

    const renderItems = () => {
        return Object.values(items).map(item => {
            return <NLStickerRow handleEvents={handleItemChanges(item.name)}
                                 maxPricePercent={item.maxPricePercent}
                                 name={item.name}
                                 streakPrice={item.streakPrice}
                                 key={item.name}
                                 currentPrice={prices[item.name]}/>
        })
    };

    React.useEffect(contentListener);

    return <Grid container direction={"column"} spacing={2}>
        <StorageMenu itemsAtom={NLStickersAtom}/>
        <Grid item container className={"header-container"}>
            <Header xs={3}>Name</Header>
            <Header xs={1}>Link</Header>
            <Header xs={2}>Current price</Header>
            <Header xs={1}>Max price(%)</Header>
            <Header xs={1}>1</Header>
            <Header xs={1}>2</Header>
            <Header xs={1}>3</Header>
            <Header xs={1}>4</Header>
            <Header xs={1}>Delete</Header>
        </Grid>
        {renderItems()}

    </Grid>
}
