import React from "react";
import { Button, Grid, IconButton } from "@mui/material";
import { Launch, DeleteForever, Update } from "@mui/icons-material";
import { ItemHandledEvent } from "./FloatTable";
import { PriceResponse } from "lib";
import { HtmlTooltip, Input, ItemColumn } from "../../ui";

type FloatItemProps = {
    name: string,
    maxPrice: number,
    minFloat: number,
    maxFloat: number,
    handleEvents: (name: string) => (event: ItemHandledEvent) => (event) => void,
    currentPrice: PriceResponse
}


export function FloatRow(props: FloatItemProps) {
    const handleCopy = async () => {
        await navigator.clipboard.writeText(props.name)
    };

    const displayCurrentPrice = () => {
        if (props.currentPrice) {
            props.currentPrice.lastPrices;
            return <HtmlTooltip
                arrow={true}
                title={<Grid container direction={"column"}>
                    <Grid item><em>{props.currentPrice.time}</em></Grid>
                    {props.currentPrice.lastPrices.map((price, index) => {
                        return <Grid item key={index}>{price}</Grid>
                    })}
                </Grid>}>
                <Button
                    onClick={props.handleEvents(props.name)("updatePrice")}>{props.currentPrice.lastPrices[0]}</Button>
            </HtmlTooltip>
        }
        return <IconButton onClick={props.handleEvents(props.name)("updatePrice")}>
            <Update/>
        </IconButton>
    };

    const link = `https://steamcommunity.com/market/listings/730/${encodeURI(props.name)}`;
    return <Grid container className={"item-container"}>
        <ItemColumn xs={4}>
            <Button onClick={handleCopy}>{props.name}</Button>
        </ItemColumn>
        <ItemColumn xs={1}>
            <IconButton href={link} target={"_blank"}>
                <Launch fontSize={"small"}/>
            </IconButton>
        </ItemColumn>
        <ItemColumn xs={2}>
            {displayCurrentPrice()}
        </ItemColumn>
        <ItemColumn xs={1}>
            <Input value={props.maxPrice}
                   step={10}
                   onBlur={props.handleEvents(props.name)("maxPrice")}/>
        </ItemColumn>
        <ItemColumn xs={1}>
            <Input value={props.minFloat}
                   onBlur={props.handleEvents(props.name)("minFloat")}
                   step={0.001}/>
        </ItemColumn>
        <ItemColumn xs={1}>
            <Input onBlur={props.handleEvents(props.name)("maxFloat")}
                   value={props.maxFloat}
                   step={0.001}/>
        </ItemColumn>
        <ItemColumn xs={1}>
            <IconButton onClick={props.handleEvents(props.name)("delete")}>
                <DeleteForever fontSize={"small"}/>
            </IconButton>
        </ItemColumn>
    </Grid>
}





