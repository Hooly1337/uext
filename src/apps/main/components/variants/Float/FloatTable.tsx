import React, { Profiler } from "react";
import { debounce, Grid } from "@mui/material";
import { Header } from "../../ui";
import { ItemInfo } from "../../../../content/main";
import { getPrice } from "lib";
import { StorageMenu } from "../../StorageMenu";
import { PricesDict } from "../../ui";
import { useAtom, useAtomValue } from "jotai";
import { currentProfileStorageNameAtom, floatItemsAtom } from "store";
import { FloatRow } from "./FloatRow";


export type ItemHandledEvent = "delete" | "minFloat" | "maxFloat" | "maxPrice" | "updatePrice"


export function FloatTable() {
    const [items, setItems] = useAtom(floatItemsAtom);

    const currentProfileStorageName = useAtomValue(currentProfileStorageNameAtom);

    const [prices, setPrices] = React.useState<PricesDict>({});

    const renderItems = () => {
        return Object.entries(items).map(([name, item]) => {
            return <FloatRow
                name={name}
                maxFloat={item.maxFloat}
                minFloat={item.minFloat}
                maxPrice={item.maxPrice}
                currentPrice={prices[name]}
                handleEvents={handleItemChange}
                key={name}
            />
        })
    };


    const saveItems = debounce(() => {
        localStorage.setItem(currentProfileStorageName, JSON.stringify(items))
    }, 2000);

    const handleItemChange = (name: string) => {
        return (event: ItemHandledEvent) => {
            switch (event) {
                case "delete":
                    return () => {
                        delete items[name];
                        saveItems();
                        setItems(items)
                    };
                case "maxFloat":
                    return (event) => {
                        items[name].maxFloat = parseFloat(event.target.value);
                        saveItems();
                        setItems(items);
                    };
                case "maxPrice":
                    return (event) => {
                        items[name].maxPrice = parseFloat(event.target.value);
                        saveItems();
                        setItems(items)
                    };
                case "minFloat":
                    return (event) => {
                        items[name].minFloat = parseFloat(event.target.value);
                        saveItems();
                        setItems(items)
                    };
                case "updatePrice":
                    return async () => {
                        prices[name] = await getPrice(name);
                        setPrices(prices)
                    }
            }

        }
    };

    const contentListener = () => {
        const listener = (itemInfo: ItemInfo) => {
            if (items[itemInfo.name] == undefined) {
                items[itemInfo.name] = {
                    name: itemInfo.name,
                    minFloat: 0,
                    maxFloat: 1,
                    maxPrice: 0
                };
                saveItems();
                setItems(items)
            }
        };
        //chrome.runtime.onMessage.addListener(listener);
        return () => {
            //chrome.runtime.onMessage.removeListener(listener)
        }
    };

    React.useEffect(contentListener);
    return (
        <Profiler id={"zalupa"} onRender={() => {
            console.log("suka")
        }}>
            <Grid container direction={"column"} spacing={2}>
                <StorageMenu itemsAtom={floatItemsAtom}/>
                <Grid item container spacing={2}>
                    <Grid item container className={"header-container"}>
                        <Header xs={4}>Name</Header>
                        <Header xs={1}>Link</Header>
                        <Header xs={2}>Current price</Header>
                        <Header xs={1}>Max price</Header>
                        <Header xs={1}>Min float</Header>
                        <Header xs={1}>Max float</Header>
                        <Header xs={1}>Remove</Header>
                    </Grid>
                    {renderItems()}
                </Grid>
            </Grid>
        </Profiler>
        )


}