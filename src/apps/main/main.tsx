import * as React from "react";
import "./App.scss";
import { App } from "./App";
import { createRoot } from "react-dom/client";


export function main() {
    const container = document.getElementById("root");

    if (!container) {
        throw new Error("root undefined")
    }
    const root = createRoot(container);
    root.render(<App/>)
}