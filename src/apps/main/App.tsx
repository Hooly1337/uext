import { Box, Grid, Select, MenuItem } from "@mui/material"
import * as React from "react";
import ProfileSelector from "./components/ProfileSelector";
import { useAtom } from "jotai";
import { currentVariantNameAtom } from "store";


export function App() {
    const [current, setCurrent] = useAtom(currentVariantNameAtom);

    const handleChange = (event: any) => {
        setCurrent(event.target.value)
    };


    return (
        <Grid container direction={"column"} spacing={2}>
            <Grid item>
                <Select value={current} onChange={handleChange} fullWidth variant={"outlined"}>
                    <MenuItem value="Floats">Float Picker</MenuItem>
                    <MenuItem value="NLStickers">Sticker Picker</MenuItem>
                </Select>
            </Grid>
            <Grid item>
                <ProfileSelector/>
            </Grid>
        </Grid>)

}