import { atomWithStorage } from "jotai/utils";
import { atom } from "jotai";

export type VariantName = "Floats" | "NLStickers"

export type VariantProfile = {
    current: string,
    all: string[]
}

export type Variants = Record<VariantName, VariantProfile> & { currentVariant: VariantName }

const variantsFactory = (): Variants => {
    return {
        currentVariant: "Floats",
        Floats: {
            current: "new",
            all: []
        },
        NLStickers: {
            current: "new",
            all: []
        }
    }
}

const variantsAtom = atomWithStorage<Variants>("variants", variantsFactory())

export const currentVariantNameAtom = atom(
    (get) => get(variantsAtom).currentVariant,

    (get, set, update: VariantName) => {
        set(variantsAtom, { ...get(variantsAtom), currentVariant: update })
    })

export const profileAtom = atom((get) => {
    const currentVariantName = get(currentVariantNameAtom);
    return get(variantsAtom)[currentVariantName];

}, (get, set, update: VariantProfile) => {

    const currentVariantName = get(currentVariantNameAtom);
    const variants = get(variantsAtom);

    set(variantsAtom, {...variants, [currentVariantName]: update})
})


export const currentProfileStorageNameAtom = atom((get)=> {
    const profileName = get(profileAtom).current;
    const variantName = get(currentVariantNameAtom);

    return `${variantName}-${profileName}`;
})

