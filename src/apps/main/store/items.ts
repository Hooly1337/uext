import { atom } from "jotai";
import { currentProfileStorageNameAtom } from "./variants";


export type FloatItem = {
    name: string;
    minFloat: number;
    maxFloat: number;
    maxPrice: number;
}

export type NLStickerItem = {
    name: string;
    maxPricePercent: number;
    streakPrice: number[];
}


export type ItemDict<T> = Record<string, T>


export const itemsAtomFactory = <T>() => {

    const currentItemsAtom = atom({});

    return atom<ItemDict<T>, ItemDict<T>>(
        (get) => {
            const storageName = get(currentProfileStorageNameAtom);
            const currentValue = get(currentItemsAtom);

            if (JSON.stringify(currentValue) == "{}") {
                const json = localStorage.getItem(storageName);

                if (json) {
                    return JSON.parse(json);
                }
            }


            return currentValue;
        }, (get, set, update) => {
            set(currentItemsAtom, { ...update });
        });
}

export const floatItemsAtom = itemsAtomFactory<FloatItem>();

export const NLStickersAtom = itemsAtomFactory<NLStickerItem>();

