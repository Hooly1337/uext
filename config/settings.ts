import { resolve } from "path";
import { sassPlugin } from "esbuild-sass-plugin";
import { BuildOptions } from "esbuild";

const mode = process.env.MODE || "development"

const isDev = mode === "development";
const isProd = mode === "production";


function resolveRoot(...segments: string[]) {
    return resolve(__dirname, '..', ...segments)
}

const settings: BuildOptions = {
    plugins: [sassPlugin()],
    outdir: resolveRoot("extension"),
    entryPoints: [resolveRoot("src/apps/background/index.ts"), resolveRoot("src/apps/main/index.ts"), resolveRoot("src/apps/content/index.ts")],
    entryNames: 'dist/[ext]/[dir]',
    allowOverwrite: true,
    bundle: true,
    minify: isProd,
    sourcemap: isDev,
    tsconfig: resolveRoot("tsconfig.json")
}

export default settings;