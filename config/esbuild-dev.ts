import esbuild from "esbuild";
import settings from "./settings";

esbuild.serve({port: 3000, servedir: settings.outdir}, settings).then(res => {
    console.log("http://localhost:3000")
})